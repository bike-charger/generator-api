package com.bikecharger.genapi.http;

import com.bikecharger.genapi.sensor.VoltageSensor;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class HttpAPI {

    HttpServer server;

    public HttpAPI(int port, final VoltageSensor voltageSensor) throws Exception {
        server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/data", new HttpHandler() {
            public void handle(HttpExchange httpExchange) throws IOException {
                String response = "{\"power\": " + voltageSensor.getPower() + ", \"speed\": " + voltageSensor.getSpeed() + "}";
                httpExchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                httpExchange.sendResponseHeaders(200, response.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(response.getBytes());
                os.close();
            }
        });
        server.setExecutor(null);
        server.start();
    }

}
