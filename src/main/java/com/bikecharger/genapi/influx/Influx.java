package com.bikecharger.genapi.influx;

import com.bikecharger.genapi.sensor.VoltageSensor;
import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;

import java.util.concurrent.TimeUnit;

public class Influx {

    InfluxDB influxDB;
    private final String DB_NAME = "bikecharger";
    private final String RP_NAME = "autogen";

    public Influx(String host, int port) {
        influxDB = InfluxDBFactory.connect("http://" + host + ":" + port);
        influxDB.enableBatch(BatchOptions.DEFAULTS.actions(2000).flushDuration(100));
    }

    public void sendData(VoltageSensor sensor) {
        final Point voltage = Point.measurement("voltage")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("value", sensor.getVoltage())
                .build();

        final Point current = Point.measurement("current")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("value", sensor.getCurrent())
                .build();

        final Point power = Point.measurement("power")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("value", sensor.getPower())
                .build();

        final Point speed = Point.measurement("speed")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("value", sensor.getSpeed())
                .build();

        new Thread(new Runnable() {
            public void run() {
                influxDB.write(DB_NAME, RP_NAME, voltage);
                influxDB.write(DB_NAME, RP_NAME, current);
                influxDB.write(DB_NAME, RP_NAME, power);
                influxDB.write(DB_NAME, RP_NAME, speed);
            }
        }).start();
    }

}
