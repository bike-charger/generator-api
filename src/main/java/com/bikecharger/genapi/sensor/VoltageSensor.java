package com.bikecharger.genapi.sensor;

import java.io.IOException;
import java.util.Random;

public class VoltageSensor {

    private boolean wasUpdated;
    private double lastVoltage;
    private double lastCurrent;
    private INA219Base base;

    private static final double VOLTAGE = 1.1859;
    private static final double CONSTANT = 0.3567;
    // R^2 = 0.9951 | n = 4

    public VoltageSensor() {
        wasUpdated = false;
        lastVoltage = 0.0;
        lastCurrent = 0.0;


        try {
            base = new INA219Base(new I2CRegisterImpl(INA219.Address.ADDR_40), 0.1, 2, INA219.Brng.V32, INA219.Pga.GAIN_8, INA219.Adc.BITS_12, INA219.Adc.BITS_12);
        } catch (IOException e) {
            System.err.println("Failed to set up voltage sensor!");
            System.exit(1);
        }

    }

    public double getVoltage() {
        if (!wasUpdated) {
            return 0.0;
        } else {
            return lastVoltage;
        }
    }

    public double getCurrent() {
        if (!wasUpdated) {
            return 0.0;
        } else {
            return lastCurrent;
        }
    }

    public double getPower() {
        return getVoltage() * getCurrent();
    }

    public double getSpeed() {
        return CONSTANT + (getVoltage() * VOLTAGE);
    }

    public void update() throws IOException {
        lastVoltage = base.getBusVoltage() + (base.getShuntVoltage()/1000);
        lastCurrent = base.getCurrent();
        //lastVoltage = 3;
        //lastCurrent = 2;
        wasUpdated = true;
    }
}
