package com.bikecharger.genapi;

import com.bikecharger.genapi.http.HttpAPI;
import com.bikecharger.genapi.influx.Influx;
import com.bikecharger.genapi.sensor.VoltageSensor;

import java.io.IOException;

public class GeneratorAPI {

    public static void main(String[] args) throws InterruptedException {
        VoltageSensor sensor = new VoltageSensor();
        HttpAPI httpAPI;
        try {
            httpAPI = new HttpAPI(3000, sensor);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        Influx influx = new Influx("35.237.43.14", 8086);


        while (true) {
            try {
                sensor.update();

                System.out.println("Read data from voltage sensor!");
                System.out.println(sensor.getVoltage() + "V   " + sensor.getCurrent() + "A   " + sensor.getPower() + "W");

                influx.sendData(sensor);
                System.out.println("Flushed data to influxdb");

                Thread.sleep(500); // run 10 times per second
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Failed to read sensor!");
            }
        }
    }

}
